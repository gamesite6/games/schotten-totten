import { maxBy } from "lodash-es";
import { describe, test, expect } from "vitest";
import {
  getSuit,
  getCardValue,
  Suit,
  getCardId,
  Combo,
  classifyCombo,
  getArmyScore,
  findWinner,
} from "./utils";

describe("utils", () => {
  test("card value", () => {
    expect(getCardValue(9)).toBe(9);
    expect(getCardValue(15)).toBe(6);
    expect(getCardValue(54)).toBe(9);
  });
  test("card suit", () => {
    expect(getSuit(9)).toBe(Suit.A);
    expect(getSuit(15)).toBe(Suit.B);
    expect(getSuit(54)).toBe(Suit.F);
  });
  test("get card id", () => {
    const card = 33;
    const suit = getSuit(card);
    const value = getCardValue(card);

    expect(suit).toBe(Suit.D);
    expect(value).toBe(6);

    expect(getCardId(value, suit)).toBe(card);
  });
  test("classify straight flush", () => {
    const cards = [
      getCardId(3, Suit.B),
      getCardId(5, Suit.B),
      getCardId(4, Suit.B),
    ];
    expect(classifyCombo(cards)).toBe(Combo.StraightFlush);
  });
  test("classify flush", () => {
    const cards = [
      getCardId(1, Suit.B),
      getCardId(2, Suit.B),
      getCardId(9, Suit.B),
    ];
    expect(classifyCombo(cards)).toBe(Combo.Flush);
  });
  test("classify straight", () => {
    const cards = [
      getCardId(7, Suit.C),
      getCardId(6, Suit.B),
      getCardId(5, Suit.E),
    ];
    expect(classifyCombo(cards)).toBe(Combo.Straight);
  });
  test("classify trips", () => {
    const cards = [
      getCardId(7, Suit.C),
      getCardId(7, Suit.B),
      getCardId(7, Suit.E),
    ];
    expect(classifyCombo(cards)).toBe(Combo.Trips);
  });
  test("classify high card", () => {
    const cards = [
      getCardId(2, Suit.C),
      getCardId(6, Suit.B),
      getCardId(5, Suit.E),
    ];
    expect(classifyCombo(cards)).toBe(Combo.HighCard);
  });

  test("compare combo", () => {
    expect(Combo.StraightFlush > Combo.Flush).toBe(true);
    expect(Combo.StraightFlush > Combo.Flush).toBe(true);
    expect(Combo.StraightFlush > Combo.Flush).toBe(true);
    expect(Combo.StraightFlush > Combo.Flush).toBe(true);
    expect(Combo.StraightFlush > Combo.Flush).toBe(true);
  });

  test("compare armies", () => {
    const armyA = {
      cards: [getCardId(7, Suit.A), getCardId(7, Suit.B), getCardId(7, Suit.C)],
      advantage: false,
    };
    const armyB = {
      cards: [getCardId(7, Suit.D), getCardId(7, Suit.E), getCardId(7, Suit.F)],
      advantage: true,
    };
    const winner = maxBy([armyA, armyB], getArmyScore);
    expect(winner).toBe(armyB);
  });
});

describe("winner", () => {
  test("has 3 connected flags", () => {
    let claims = [null, 1, 1, 1, null, 2, null];
    expect(findWinner(claims)).toBe(1);

    claims = [null, null, 1, 1, null, 2, null];
    expect(findWinner(claims)).toBeNull();
  });

  test("has 5 flags", () => {
    let claims: Claims = [1, 2, 2, 1, 2, 1, 2, 2, 1];
    expect(findWinner(claims)).toBe(2);

    claims = [1, null, 2, 1, 2, 1, 2, 2, 1];
    expect(findWinner(claims)).toBeNull();
  });
});
