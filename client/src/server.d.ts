type PlayerId = number;
type CardId = number;
type BattleIndex = number;
type Claims = (PlayerId | null)[];

type GameState = {
  players: [PlayerState, PlayerState];
  deck: CardId[];
  phase: Phase;
  claims: Claims;
  advantages: (PlayerId | null)[];
};

type PlayerState = {
  id: PlayerId;
  hand: CardId[];
  placed: CardId[][];
};

type FailedClaim = [BattleIndex, CardId[]];

type Phase =
  | ["placing", PlayerId, FailedClaim | null]
  | ["drawing", PlayerId, FailedClaim | null]
  | ["ended"];

type GameSettings = {};

type Action =
  | ["place", CardId, BattleIndex]
  | ["draw"]
  | ["claim", BattleIndex]
  | ["skip"];
