import { createEventDispatcher } from "svelte";

export function isPlayerActive(phase: Phase, playerId: PlayerId): boolean {
  switch (phase[0]) {
    case "placing":
    case "drawing":
      return phase[1] === playerId;
    case "ended":
      return false;
  }
}

export function createActionDispatcher() {
  const dispatch = createEventDispatcher();
  return (action: Action) => {
    dispatch("action", action);
  };
}

export function zip<A, B>(as: A[], bs: B[]): [A, B][] {
  return as.map((a, i) => [a, bs[i]]);
}

export enum Suit {
  A = "a",
  B = "b",
  C = "c",
  D = "d",
  E = "e",
  F = "f",
}

const suits = Object.values(Suit);

export function getSuit(card: CardId): Suit {
  const idx = card - 1;

  return suits[Math.floor(idx / 9)];
}

export function getCardValue(card: CardId): number {
  const idx = card - 1;
  return (idx % 9) + 1;
}

export function getCardId(value: number, suit: Suit) {
  return suits.indexOf(suit) * 9 + value;
}

export enum Combo {
  HighCard,
  Straight,
  Flush,
  Trips,
  StraightFlush,
}

export type Card = { suit: Suit; value: number };
export function getCard(cardId: CardId): Card {
  return {
    suit: getSuit(cardId),
    value: getCardValue(cardId),
  };
}

export function classifyCombo(cardIds: CardId[]): Combo {
  const cards = cardIds.map((cardId) => getCard(cardId));

  const straight = isStraight(cards);
  const flush = isFlush(cards);
  const trips = isTrips(cards);

  if (straight && flush) {
    return Combo.StraightFlush;
  } else if (trips) {
    return Combo.Trips;
  } else if (flush) {
    return Combo.Flush;
  } else if (straight) {
    return Combo.Straight;
  } else {
    return Combo.HighCard;
  }
}

function isTrips(cards: Card[]): boolean {
  return cards.every((card) => card.value === cards[0].value);
}

function isFlush(cards: Card[]): boolean {
  return cards.every((card) => card.suit === cards[0].suit);
}

function isStraight(cards: Card[]): boolean {
  let [prev, ...rest] = [...cards].sort((a, b) => a.value - b.value);

  for (let card of rest) {
    if (card.value !== prev.value + 1) {
      return false;
    }
    prev = card;
  }
  return true;
}

type Army = { cards: CardId[]; advantage: boolean };

export function getArmyScore(army: Army) {
  const combo = classifyCombo(army.cards);

  let score: number = combo * 100;
  army.cards
    .map((cardId) => getCardValue(cardId))
    .forEach((value) => {
      score += value;
    });

  if (army.advantage) {
    score += 0.5;
  }
  return score;
}

export function wonBattle(
  state: GameState,
  playerId: PlayerId,
  battleIdx: BattleIndex
): boolean | undefined {
  const cards = state.players.find((p) => p.id === playerId)!.placed[battleIdx];

  if (cards.length < 3) return;

  const opponent = state.players.find((p) => p.id !== playerId)!;
  const opponentCards = opponent.placed[battleIdx];

  if (opponentCards.length < 3) return;

  const score = getArmyScore({
    cards,
    advantage: state.advantages[battleIdx] === playerId,
  });
  const opponentScore = getArmyScore({
    cards: opponentCards,
    advantage: state.advantages[battleIdx] === opponent.id,
  });

  return score > opponentScore;
}

export function anyLegalPlacement(
  player: PlayerState,
  claims: Claims
): boolean {
  return player.placed.some((cards, idx) => {
    return cards.length < 3 && claims[idx] === null;
  });
}

export function findWinner(claims: Claims): PlayerId | null {
  let consecutive: number = 1;
  const total: { [playerId: number]: number } = {};

  let previous = null;
  for (let pid of claims) {
    if (pid !== null) {
      if (pid === previous) {
        consecutive += 1;
        if (consecutive === 3) {
          return pid;
        }
      } else {
        consecutive = 1;
      }

      total[pid] = (total[pid] ?? 0) + 1;
      if (total[pid] === 5) {
        return pid;
      }
    }
    previous = pid;
  }

  return null;
}
