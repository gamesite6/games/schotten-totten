import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: [{ find: /^src/, replacement: `${__dirname}/src` }],
  },
  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/index.ts",
      fileName: "border-brawl",
      name: "Gamesite6_BorderBrawl",
    },
  },
  plugins: [svelte()],
});
