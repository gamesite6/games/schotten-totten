# frozen_string_literal: true

require 'json'

class Phase
  def self.from(arr)
    phase_name, *phase_data = arr
    case phase_name
    when Placing.phase_name
      Placing.new(player: phase_data[0], failed_claim: phase_data[1])
    when Drawing.phase_name
      Drawing.new(player: phase_data[0], failed_claim: phase_data[1])
    when Ended.phase_name
      Ended.new
    end
  end
end

class Placing < Phase
  def self.phase_name
    'placing'
  end

  attr_accessor :player, :failed_claim

  def initialize(player:, failed_claim: nil)
    @player = player
    @failed_claim = failed_claim
  end

  def to_json(_options = {})
    [self.class.phase_name, player, failed_claim].to_json
  end
end

class Drawing < Phase
  def self.phase_name
    'drawing'
  end

  attr_accessor :player, :failed_claim

  def initialize(player:, failed_claim:)
    @player = player
    @failed_claim = failed_claim
  end

  def to_json(_options = {})
    [self.class.phase_name, player, failed_claim].to_json
  end
end

class FailedClaim
  attr_reader :battle_idx, :example_combo

  def initialize(battle_idx:, example_combo:)
    @battle_idx = battle_idx
    @example_combo = example_combo
  end

  def to_json(_options = {})
    [battle_idx, example_combo.cards].to_json
  end
end

class Ended < Phase
  def self.phase_name
    'ended'
  end

  def to_json(_options = {})
    [self.class.phase_name].to_json
  end
end
