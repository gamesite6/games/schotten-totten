# frozen_string_literal: true

require 'json'

class Action
  attr_reader :user_id

  def initialize(user_id:)
    @user_id = user_id
  end

  def self.from(user_id:, action:)
    case action[0]
    when 'place'
      Place.new(user_id:, card: action[1], battle: action[2])
    when 'draw'
      Draw.new(user_id:)
    when 'claim'
      Claim.new(user_id:, battle: action[1])
    when 'skip'
      Skip.new(user_id:)
    end
  end
end

class Place < Action
  attr_reader :card, :battle

  def initialize(user_id:, card:, battle:)
    super(user_id:)
    @card = card
    @battle = battle
  end

  def legal?(game_state)
    user = game_state.find_player(user_id)
    game_state.phase.is_a?(Placing) &&
      game_state.phase.player == user_id &&
      game_state.claims[battle].nil? &&
      user.placed[battle].size < 3 &&
      user.hand.include?(card)
  end
end

class Draw < Action
  def legal?(game_state)
    game_state.phase.is_a?(Drawing) &&
      game_state.phase.player == user_id
  end
end

class Claim < Action
  attr_reader :battle

  def initialize(user_id:, battle:)
    super(user_id:)
    @battle = battle
  end

  def legal?(game_state)
    game_state.phase.player == user_id &&
      game_state.claims[battle].nil? &&
      game_state.find_army(player_id: user_id, battle_idx: battle)&.ready? &&
      game_state.phase &&
      !game_state.has_failed_attempt?
  end
end

class Skip < Action
  def legal?(game_state)
    user = game_state.find_player(user_id)
    game_state.phase.is_a?(Placing) &&
      game_state.phase.player == user_id &&
      user.placed.zip(game_state.claims).all? do |cards, claim|
        cards.size >= 3 || !claim.nil?
      end
  end
end
