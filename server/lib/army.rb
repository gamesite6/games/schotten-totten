require_relative 'combo'

class Army
  attr_accessor :cards, :advantage, :required_size

  def initialize(cards:, advantage: false, required_size: 3)
    @cards = cards
    @advantage = advantage
    @required_size = required_size
  end

  def size
    cards.size
  end

  def ready?
    size == required_size
  end

  def can_claim_victory_over?(other, available_cards:)
    return false unless ready?

    combo = Combo.new(cards:)

    other_combo = other.best_possible_combo(available_cards:)

    return true, nil if other_combo.nil?

    if advantage
      [combo >= other_combo, other_combo]
    else
      [combo > other_combo, other_combo]
    end
  end

  def best_possible_flush(available_cards:, cards: self.cards)
    if cards.empty?
      available_cards
        .group_by(&:suit)
        .filter_map do |_, grp|
          best_possible_flush(
            cards: grp,
            available_cards: available_cards - grp
          )
        end
        .max

    else
      return nil unless ComboUtils.all_same_suit?(cards)

      suit = cards.first.suit

      combo = cards +
              available_cards
              .filter { |c| c.suit == suit }
              .sort_by { |c| -c.value }
              .take(required_size - cards.size)

      return Combo.new(cards: combo) if combo.size == required_size
    end
  end

  def best_possible_straight(available_cards:)
    values = cards.map(&:value)

    useful_cards = available_cards
                   .reject { |c| values.include?(c.value) }
                   .uniq(&:value)

    (cards + useful_cards)
      .sort_by { |c| -c.value }
      .slice_when { |i, j| i.value != j.value + 1 }
      .flat_map { |grp| grp.each_cons(required_size).to_a }
      .find { |grp| cards.all? { |c| grp.include?(c) } }
      &.then { |combo| Combo.new(cards: combo) }
  end

  def best_possible_n_of_a_kind(available_cards:)
    if cards.empty?
      combo = available_cards
              .group_by(&:value)
              .sort_by { |k, _| -k }
              .find { |_, grp| grp.size >= required_size }
              &.at(1)
              &.take(required_size)

      return Combo.new(cards: combo) if combo && combo.size == required_size

    else
      value = cards.first.value
      return nil unless ComboUtils.all_same_value?(cards)

      same_value_cards = available_cards
                         .filter { |c| c.value == value }

      combo = (cards + same_value_cards).take(required_size)

      return Combo.new(cards: combo) unless combo.size < required_size
    end
  end

  def best_possible_straight_flush(available_cards:)
    if cards.empty?

      available_cards
        .group_by(&:suit)
        .filter_map do |_, grp|
          best_possible_straight(available_cards: grp)
        end
        .max

    else
      suit = cards.first.suit
      return nil unless ComboUtils.all_same_suit?(cards)

      suited_available = available_cards.filter { |c| c.suit == suit }

      best_possible_straight(available_cards: suited_available)
    end
  end

  def best_possible_high_cards(available_cards:)
    take_size = required_size - size

    combo = cards + available_cards.sort_by { |c| -c.value }
                                   .take(take_size)

    return Combo.new(cards: combo) unless combo.size < required_size
  end

  def best_possible_combo(available_cards:)
    best_possible_straight_flush(available_cards:) ||
      best_possible_n_of_a_kind(available_cards:) ||
      best_possible_flush(available_cards:) ||
      best_possible_straight(available_cards:) ||
      best_possible_high_cards(available_cards:)
  end
end
