# frozen_string_literal: true

require 'json'
require_relative 'action'
require_relative 'phase'
require_relative 'army'
require_relative 'card'

def find_winner(claims:)
  claims
    .slice_when { |i, j| i != j }
    .find { |grp| grp.size >= 3 }
    &.first ||
    claims.group_by { |c| c }
          .find { |_, grp| grp.size >= 5 }
          &.first
end

class GameState
  class << self
    def generate(player_ids:, rng:, settings: nil)
      deck = (1..54).to_a.shuffle(random: rng)
      players = player_ids.map { |player_id| Player.new(id: player_id, hand: deck.pop(6)) }
      phase = Placing.new(player: player_ids.sample(random: rng))
      new deck:, players:, phase:
    end

    def from(hash)
      deck = hash['deck']
      players = hash['players'].map { |p| Player.from(p) }
      phase = Phase.from hash['phase']
      claims = hash['claims']
      advantages = hash['advantages']
      new deck:, players:, phase:, claims:, advantages:
    end
  end

  attr_accessor :deck, :players, :phase, :claims, :advantages

  def initialize(deck:, players:, phase:, claims: Array.new(9) { nil }, advantages: Array.new(9) { nil })
    @deck = deck
    @players = players
    @phase = phase
    @claims = claims
    @advantages = advantages
  end

  def to_json(_options = {})
    { players:, deck:, phase:, claims:, advantages: }.to_json
  end

  def apply_action(action)
    return nil unless action.legal?(self)

    case action
    when Place
      apply_place_action action
    when Draw
      apply_draw_action action
    when Claim
      apply_claim_action action
    when Skip
      apply_skip_action action
    end

    self
  end

  def player_army(player:, battle_idx:)
    Army.new(
      cards: player.placed[battle_idx].map { |c| Card.new(c) },
      advantage: advantages[battle_idx] == player.id
    )
  end

  def available_cards
    (deck + players.flat_map { |p| p.hand })
      .map { |c| Card.new(c) }
  end

  def find_player_and_opponent(player_id:)
    player = players.find { |p| p.id == player_id }
    opponent = players.find { |p| p.id != player_id }
    [player, opponent]
  end

  def find_player(player_id)
    players.find { |p| p.id == player_id }
  end

  def has_failed_attempt?
    phase.failed_claim if phase.respond_to?(:failed_claim)
  end

  def find_army(player_id:, battle_idx:)
    find_player(player_id)
      &.then do |player|
        player_army(player:, battle_idx:)
      end
  end

  private

  def apply_place_action(action)
    user = players.find { |p| p.id == action.user_id }

    user.place_card(action.card, action.battle)
    advantages[action.battle] ||= user.id if user.placed[action.battle].size >= 3
    self.phase = Drawing.new(player: user.id, failed_claim: phase.failed_claim)
  end

  def apply_draw_action(action)
    user = players.find { |p| p.id == action.user_id }
    opponent = players.find { |p| p.id != action.user_id }

    card = deck.pop
    user.hand.push(card) if card
    self.phase = Placing.new(player: opponent.id)
  end

  def apply_claim_action(action)
    battle_idx = action.battle
    player, opponent = find_player_and_opponent(player_id: action.user_id)

    army = player_army(player:, battle_idx:)
    opponent_army = player_army(player: opponent, battle_idx:)

    ok, example_combo = army.can_claim_victory_over?(opponent_army, available_cards:)

    if ok
      claims[action.battle] = action.user_id
      self.phase = Ended.new if find_winner(claims:)
    else
      phase.failed_claim = FailedClaim.new(battle_idx:, example_combo:)
    end
  end

  def apply_skip_action(action)
    user = players.find { |p| p.id == action.user_id }
    self.phase = Drawing.new(player: user.id, failed_claim: phase.failed_claim)
  end

  def all_placed_cards
    players.flat_map(&:placed).flatten
  end
end

class Player
  attr_accessor :id, :hand, :placed

  def initialize(id:, hand:, placed: Array.new(9) { [] })
    @id = id
    @hand = hand
    @placed = placed
  end

  def to_json(_options = {})
    { id:, hand:, placed: }.to_json
  end

  def self.from(hash)
    id = hash['id']
    hand = hash['hand']
    placed = hash['placed']

    new id:, hand:, placed:
  end

  def place_card(card, battle_idx)
    hand.delete(card)
    (placed[battle_idx] ||= []).push(card)
  end

  def battle_cards(battle_idx)
    placed[battle_idx] || []
  end
end
