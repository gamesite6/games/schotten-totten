# frozen_string_literal: true

require 'json'

class Card
  class << self
    def from_card_id(card_id)
      new card_id
    end

    def of!(value:, suit:)
      card_id = suits.index(suit) * 9 + value
      new card_id
    end

    def of(value:, suit:)
      return nil unless suits.include?(suit)
      return nil if value < 1 || value > 9

      of!(value:, suit:)
    end

    def suits
      %w[a b c d e f]
    end

    def all
      (1..54).map { |card_id| Card.new(card_id) }
    end
  end

  suits.each do |suit|
    (1..9).each do |value|
      define_singleton_method "#{suit}#{value}" do
        Card.of!(value:, suit:)
      end
    end
  end

  attr_reader :card_id

  def initialize(card_id)
    @card_id = card_id
  end

  def suit
    @suit ||= Card.suits[(card_id - 1) / 9]
  end

  def value
    @value ||= ((card_id - 1) % 9) + 1
  end

  def ==(other)
    card_id == other.card_id
  end

  def to_s
    "#{suit}#{value}"
  end

  def to_json(_options = {})
    card_id.to_json
  end
end
