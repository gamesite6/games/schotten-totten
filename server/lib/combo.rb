# frozen_string_literal: true

module ComboUtils
  class << self
    def connected_values?(cards)
      cards
        .map(&:value)
        .sort
        .each_cons(2)
        .all? { |a, b| a + 1 == b }
    end

    def all_same_value?(cards)
      cards
        .map(&:value)
        .uniq
        .size < 2
    end

    def all_same_suit?(cards)
      cards
        .map(&:suit)
        .uniq
        .size < 2
    end
  end
end

class Combo
  attr_reader :cards

  def initialize(cards:)
    @cards = cards
  end

  def straight?
    ComboUtils.connected_values?(cards)
  end

  def flush?
    ComboUtils.all_same_suit?(cards)
  end

  def straight_flush?
    straight? && flush?
  end

  def n_of_a_kind?
    ComboUtils.all_same_value?(cards)
  end

  def score
    score =
      if straight_flush?
        400
      elsif n_of_a_kind?
        300
      elsif flush?
        200
      elsif straight?
        100
      else
        0
      end

    score + cards.map(&:value).sum
  end

  def to_s
    cards.sort_by { |c| "#{c.value}#{c.suit}" }
         .map(&:to_s)
         .join(', ')
  end

  include Comparable

  def <=>(other)
    score <=> other.score
  end
end
