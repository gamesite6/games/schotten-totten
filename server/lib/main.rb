# frozen_string_literal: true

require 'agoo'
require 'oj'
require_relative 'game_state'
require_relative 'action'

Oj.mimic_JSON
Oj.add_to_json(Array, Hash, Integer)

PORT = Integer(ENV['PORT'] || (raise 'missing PORT env var'))

Agoo::Server.init(PORT, 'root')

class InfoHandler
  def call(_req)
    res_body = Oj.dump({ playerCounts: [2] })
    [200, { 'Content-Type' => 'application/json' }, [res_body]]
  end
end

class InitialStateHandler
  def call(req)
    req_body = Oj.load req['rack.input'].read

    players, settings, seed = req_body.values_at('players', 'settings', 'seed')
    rng = Random.new(Integer(seed))

    res_body = Oj.dump({ state: GameState.generate(player_ids: players, settings:, rng:) })

    [200, { 'Content-Type' => 'application/json' }, [res_body]]
  end
end

class PerformActionHandler
  def call(req)
    req_body = Oj.load req['rack.input'].read

    state, action, _settings, performed_by, _seed = req_body.values_at('state', 'action', 'settings',
                                                                       'performedBy', 'seed')
    state = GameState.from(state)
    action = Action.from(user_id: performed_by, action:)

    next_state = state.apply_action action

    if next_state
      res_body = Oj.dump({ nextState: next_state, completed: false })
      [200, { 'Content-Type' => 'application/json' }, [res_body]]
    else
      [422, {}, ['invalid action']]
    end
  end
end

Agoo::Server.handle(:POST, '/info', InfoHandler.new)
Agoo::Server.handle(:POST, '/initial-state', InitialStateHandler.new)
Agoo::Server.handle(:POST, '/perform-action', PerformActionHandler.new)

Agoo::Server.start
