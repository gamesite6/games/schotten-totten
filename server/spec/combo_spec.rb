# frozen_string_literal: true

require 'combo'
require 'army'

straight = Combo.new(cards: [Card.a3, Card.d5, Card.c4])
flush = Combo.new(cards: [Card.b3, Card.b9, Card.b2])
straight_flush = Combo.new(cards: [Card.e3, Card.e2, Card.e1])
trips = Combo.new(cards: [Card.c7, Card.a7, Card.f7])
no_combo = Combo.new(cards: [Card.c1, Card.a4, Card.a8])
empty = Combo.new(cards: [])

describe 'straight' do
  it 'has connected values' do
    expect(straight.straight?).to be_truthy
    expect(flush.straight?).to be_falsy
    expect(straight_flush.straight?).to be_truthy
  end

  it 'is 4th best combo' do
    expect(straight).to be < straight_flush
    expect(straight).to be < trips
    expect(straight).to be < flush
    expect(straight).to be > no_combo
  end
end

describe 'flush' do
  it 'has all cards of same suit' do
    expect(straight.flush?).to be_falsy
    expect(flush.flush?).to be_truthy
    expect(straight_flush.flush?).to be_truthy
  end

  it 'is third best combo' do
    expect(flush).to be < straight_flush
    expect(flush).to be < trips
    expect(flush).to be > straight
    expect(flush).to be > no_combo
  end
end

describe 'straight flush' do
  it 'has connected values of same suit' do
    expect(straight.straight_flush?).to be_falsy
    expect(flush.straight_flush?).to be_falsy
    expect(straight_flush.straight_flush?).to be_truthy
  end

  it 'is the greatest combo' do
    expect(straight_flush).to be > trips
    expect(straight_flush).to be > flush
    expect(straight_flush).to be > straight
    expect(straight_flush).to be > no_combo
  end
end

describe 'n of a kind' do
  it 'has all same value' do
    expect(trips.n_of_a_kind?).to be_truthy
  end

  it 'is second best combo' do
    expect(trips).to be < straight_flush
    expect(trips).to be > straight
    expect(trips).to be > flush
    expect(trips).to be > no_combo
  end
end

describe 'high cards (no combo)' do
  it 'is the worst combo' do
    expect(no_combo).to be < straight_flush
    expect(no_combo).to be < straight
    expect(no_combo).to be < flush
    expect(no_combo).to be < trips
  end
end
