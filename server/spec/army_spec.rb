# frozen_string_literal: true

require 'army'

describe 'army: best possible flush' do
  it 'has highest valued cards that match existing cards suit' do
    army = Army.new(cards: [Card.d3])
    available_cards = [Card.d1, Card.d9, Card.d2]

    expect(army.best_possible_flush(available_cards:).to_s).to eq('d2, d3, d9')
  end

  it 'has the highest valued available suit, if army is empty' do
    army = Army.new(cards: [])
    available_cards = [
      Card.d1, Card.d9, Card.d2,
      Card.e2, Card.e8, Card.e5
    ]

    expect(army.best_possible_flush(available_cards:).to_s).to eq('e2, e5, e8')
  end
end

describe 'army: best possible straight' do
  it 'has the highest valued available cards' do
    army = Army.new(cards: [])
    available_cards = [Card.a2, Card.e3, Card.c5, Card.d6, Card.f7, Card.b8]

    combo = army.best_possible_straight(available_cards:)

    expect(combo.straight?).to be_truthy
    expect(combo.to_s).to eq('d6, f7, b8')
  end

  it 'fills hole in cards' do
    army = Army.new(cards: [Card.e2, Card.b4])
    available_cards = [Card.f3]
    combo = army.best_possible_straight(available_cards:)
    expect(combo.to_s).to eq('e2, f3, b4')
  end

  it 'takes highest values from available cards' do
    army = Army.new(cards: [Card.f6])
    available_cards = [Card.e1, Card.b2, Card.a3, Card.a5, Card.c7, Card.e8]
    combo = army.best_possible_straight(available_cards:)
    expect(combo.to_s).to eq('f6, c7, e8')
  end

  it 'must include the army cards' do
    army = Army.new(cards: [Card.a3])
    available_cards = [Card.e1, Card.b2, Card.a6, Card.c7, Card.e8]
    combo = army.best_possible_straight(available_cards:)
    expect(combo.to_s).to eq('e1, b2, a3')
  end

  it 'must meet required size' do
    army = Army.new(cards: [Card.b2, Card.a3, Card.b4], required_size: 4)
    available_cards = [Card.c6]
    combo = army.best_possible_straight(available_cards:)
    expect(combo).to be_nil
  end
end

describe 'army: best possible n of a kind' do
  it 'takes highest valued cards if army is empty' do
    army = Army.new(cards: [])
    available_cards = [Card.e1, Card.b1, Card.a2, Card.c1, Card.e2, Card.f2]
    combo = army.best_possible_n_of_a_kind(available_cards:)
    expect(combo.to_s).to eq('a2, e2, f2')
  end

  it 'takes cards that matches existing value' do
    army = Army.new(cards: [Card.e1])
    available_cards = [Card.b1, Card.a2, Card.c1, Card.e2, Card.f2]
    combo = army.best_possible_n_of_a_kind(available_cards:)
    expect(combo.to_s).to eq('b1, c1, e1')
  end
end

describe 'army: best possible straight flush' do
  it 'takes highest valued cards if army is empty' do
    army = Army.new(cards: [])
    available_cards = [Card.e1, Card.e3, Card.e2, Card.c4, Card.c3, Card.c2]
    combo = army.best_possible_straight_flush(available_cards:)
    expect(combo.to_s).to eq('c2, c3, c4')
  end

  it 'takes highest valued cards that matches existing suit' do
    army = Army.new(cards: [Card.e2])
    available_cards = [Card.e1, Card.e3, Card.c4, Card.c3, Card.c2]
    combo = army.best_possible_straight_flush(available_cards:)
    expect(combo.to_s).to eq('e1, e2, e3')
  end

  it 'must meet required size' do
    army = Army.new(cards: [Card.e2, Card.e3], required_size: 3)
    available_cards = [Card.c4]
    combo = army.best_possible_straight_flush(available_cards:)
    expect(combo).to be_nil
  end
end

describe 'army: best possible high cards' do
  it 'fills hand with highest value available cards' do
    army = Army.new(cards: [Card.b3, Card.c7])
    available_cards = [Card.e9]
    combo = army.best_possible_high_cards(available_cards:)
    expect(combo.to_s).to eq('b3, c7, e9')
  end
end

describe 'army: claiming flag' do
  it 'cannot claim if hand is not finished' do
    army = Army.new(cards: [Card.a4, Card.a5])
    other = Army.new(cards: [Card.b3, Card.c7, Card.d1])
    available_cards = [Card.a6]

    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_falsy
  end

  it 'can claim with straight if opponent can only make high cards' do
    army = Army.new(cards: [Card.a4, Card.d5, Card.c6])
    other = Army.new(cards: [Card.b3, Card.c7])
    available_cards = [Card.e9]

    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_truthy
  end

  it 'with straight, cannot claim if opponent can make flush' do
    army = Army.new(cards: [Card.a4, Card.d5, Card.c6])
    other = Army.new(cards: [Card.b3, Card.b7])
    available_cards = [Card.b9]

    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_falsy
  end

  it 'with straight, cannot claim if opponent can make better straight' do
    army = Army.new(cards: [Card.a4, Card.d5, Card.c6])
    other = Army.new(cards: [Card.b5, Card.b7])
    available_cards = [Card.f6]

    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_falsy
  end

  it 'with advantage, can claim over other equal combo' do
    army = Army.new(cards: [Card.a4, Card.a5, Card.a6])
    other = Army.new(cards: [Card.b4, Card.b5, Card.b6])
    available_cards = []

    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_falsy

    army.advantage = true

    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_truthy
  end

  it 'with 3 of a kind, cannot claim if opponent can make straight flush' do
    army = Army.new(cards: [Card.c7, Card.e7, Card.f7])
    other = Army.new(cards: [Card.b1, Card.b2])
    available_cards = [Card.b3, Card.b4]

    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_falsy
    available_cards = [Card.b4]
    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_truthy
  end

  it 'with advantage, and the best straight flush, can always win' do
    army = Army.new(cards: [Card.c7, Card.c8, Card.c9], advantage: true)
    other = Army.new(cards: [])
    available_cards = (1..54).map { |card_id| Card.new(card_id) }

    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_truthy
    army.advantage = false
    ok, = army.can_claim_victory_over?(other, available_cards:)
    expect(ok).to be_falsy
  end
end
