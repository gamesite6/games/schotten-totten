# frozen_string_literal: true

require 'card'

describe 'card' do
  it 'has value' do
    expect(Card.new(9).value).to eq(9)
    expect(Card.new(15).value).to eq(6)
    expect(Card.new(54).value).to eq(9)
  end

  it 'has suit' do
    expect(Card.new(1).suit).to eq('a')
    expect(Card.new(9).suit).to eq('a')
    expect(Card.new(10).suit).to eq('b')
    expect(Card.new(15).suit).to eq('b')
    expect(Card.new(54).suit).to eq('f')
  end

  it 'can get card id from suit+value' do
    expect(Card.of!(suit: 'a', value: 1).card_id).to eq(1)
    expect(Card.of!(suit: 'b', value: 9).card_id).to eq(18)
    expect(Card.of!(suit: 'f', value: 9).card_id).to eq(54)
  end

  it 'nil if invalid suit+value' do
    expect(Card.of(suit: 'e', value: 0)).to be_nil
  end
end
