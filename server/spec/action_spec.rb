# frozen_string_literal: true

require 'action'

describe 'action' do
  it 'can be created from array' do
    user_id = 1
    place_action = Action.from(user_id:, action: ['place', 40, 3])
    expect(place_action).to be_a(Place)

    draw_action = Action.from(user_id:, action: ['draw'])
    expect(draw_action).to be_a(Draw)

    claim_action = Action.from(user_id:, action: ['claim', 3])
    expect(claim_action).to be_a(Claim)

    skip_action = Action.from(user_id:, action: ['skip'])
    expect(skip_action).to be_a(Skip)
  end
end
