require 'game_state'
require 'json'

describe 'game state' do
  it 'can be generated from seed' do
    state = GameState.generate(player_ids: [1, 2], rng: Random.new(123))
    expect(state).to be_a(GameState)
  end

  it 'can be restored from json' do
    json = <<~EOM
      {
        "players": [
          {
            "id": 1,
            "hand": [35, 14, 15, 33, 42, 2],
            "placed": [[], [], [], [], [], [], [], [], []]
          },
          {
            "id": 2,
            "hand": [49, 51, 37, 10, 45, 27],
            "placed": [[], [], [], [], [], [], [], [], []]
          }
        ],
        "deck": [
          28, 6, 48, 41, 13, 38, 20, 34, 44, 31, 26, 54, 53, 25, 47, 7, 19, 22, 29,
          43, 32, 24, 17, 21, 52, 18, 30, 11, 23, 46, 9, 3, 39, 50, 8, 16, 40, 1, 36,
          12, 4, 5
        ],
        "phase": ["placing", 1, null],
        "claims": [null, null, null, null, null, null, null, null, null],
        "advantages": [null, null, null, null, null, null, null, null, null]
      }
    EOM
    hash = JSON.parse(json)
    state = GameState.from(hash)

    expect(state.players.size).to eq(2)
    expect(state.phase).to be_a(Placing)
  end
end

describe 'winner' do
  it 'has 3 connected flags' do
    claims = [nil, 1, 1, 1, nil, 2, nil]
    expect(find_winner(claims:)).to eq(1)

    claims = [nil, nil, 1, 1, nil, 2, nil]
    expect(find_winner(claims:)).to be_nil
  end

  it 'has 5 flags' do
    claims = [1, 2, 2, 1, 2, 1, 2, 2, 1]
    expect(find_winner(claims:)).to eq(2)

    claims = [1, nil, 2, 1, 2, 1, 2, 2, 1]
    expect(find_winner(claims:)).to be_nil
  end
end
